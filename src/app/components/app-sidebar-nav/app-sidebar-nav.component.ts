import { Component } from '@angular/core';
import { LoginService } from '../../services/login-service.service';

@Component({
  selector: 'app-sidebar-nav',
  templateUrl: './app-sidebar-nav.html'
})

export class AppSidebarNavComponent {

  constructor(private loginService: LoginService) { }

  public admin = this.loginService.isGlobalAdmin();

}

