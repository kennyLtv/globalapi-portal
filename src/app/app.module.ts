// Common
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LocationStrategy } from '@angular/common';

// Routing Module
import { AppRoutingModule } from './app.routing';

// Tabs & Dropdown
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Browser
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Progress Bar, HTTP & Interceptors
import { NgHttpLoaderModule } from 'ng-http-loader/ng-http-loader.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// 404
import { Page404Component } from './views/pages/404/404.component';

// Toastr & Notifications
import { ToastrModule } from 'ngx-toastr';
import { NotificationService } from './services/notification.service';

// XSRF Interceptor
import { XSRFInterceptor } from './services/xsrfinterceptor.service';

// Login Service
import { LoginService } from './services/login-service.service';
import { LoginComponent } from './views/pages/login/login.component';

// CoreUI App Components

import {
  AppBreadcrumbsComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  AppSidebarNavComponent
} from './components';

const APP_COMPONENTS = [
  AppBreadcrumbsComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  AppSidebarNavComponent
];

import {
  ReplaceDirective,
  NAV_DROPDOWN_DIRECTIVES,
  SIDEBAR_TOGGLE_DIRECTIVES
} from './directives';

const APP_DIRECTIVES = [
  ReplaceDirective,
  NAV_DROPDOWN_DIRECTIVES,
  SIDEBAR_TOGGLE_DIRECTIVES
];

import { FullLayoutComponent } from './containers';

const APP_CONTAINERS = [ FullLayoutComponent ];

// ------------------ MODULE ----------------- //

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgHttpLoaderModule,
    BrowserAnimationsModule,
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
    ToastrModule.forRoot({ maxOpened: 1, autoDismiss: true})
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    Page404Component,
    ...APP_CONTAINERS,
    ...APP_COMPONENTS,
    ...APP_DIRECTIVES
  ],
  providers: [
    LoginService,
    NotificationService,
    { provide: HTTP_INTERCEPTORS, useClass: XSRFInterceptor, multi: true }
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
