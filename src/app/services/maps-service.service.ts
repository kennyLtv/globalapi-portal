import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MapsService {

  constructor(private http: HttpClient) { }

  private publicAPI:string = "https://kztimerglobal.com/api/v1/maps";
  private baseUrl:string = "https://auth.global-api.com/api/v1/maps";
  private limit:number = 100;
  private difficulty:number = undefined;
  private validated:boolean = undefined;
  private mapname:string = undefined;

  set mapLimit (limit:number) {
    this.limit = limit;
  }

  get mapLimit() {
    return this.limit;
  }

  set mapDifficulty (difficulty:number) {
    this.difficulty = difficulty;
  }

  get mapDifficulty() {
    return this.difficulty;
  }

  set mapValidated (validated:boolean) {
    this.validated = validated;
  }

  get mapValidated() {
    return this.validated;
  }

  set mapName (name:string) {
    this.mapname = name;
  }

  get mapName() {
    return this.mapname;
  }

  getMaps() {

    var requestUrl:string = this.publicAPI + "?";

    if (this.difficulty != undefined) {
      requestUrl += "difficulty=" + this.difficulty + "&";
    }

    if (this.validated != undefined) {
      requestUrl += "is_validated=" + this.validated + "&";
    }

    if (this.mapname != undefined) {
      requestUrl += "name=" + this.mapname + "&";
    }

    if (this.limit > 0) {
      requestUrl += "limit=" + this.limit;
    }

    return this.http.get(requestUrl);
  }

  modifyMap(id:number, name:string, filesize:number, approver:string, difficulty:number, validated:boolean) {

    let requestUrl = this.baseUrl;
    let data:Object = {"id": id, "name": name, "validated": validated, "filesize": filesize, "difficulty": difficulty, "approved_by": approver};

    if (id > 0) {
      requestUrl += "/" + id;
    }

    return this.http.put(requestUrl, data, { withCredentials: true });
  }

  deleteMap(id:number) {

    let requestUrl = this.baseUrl;

    if (id > 0) {
      requestUrl += "/" + id;
    }

    return this.http.delete(requestUrl, { withCredentials: true});
  }

  addMap(name:string, filesize:number, approver:string, difficulty:number, validated:boolean) {

    let requestUrl = this.baseUrl;
    let data:Object = {"name": name, "validated": validated, "filesize": filesize, "difficulty": difficulty, "approved_by": approver};

    return this.http.post(requestUrl, data, { withCredentials: true});
  }
}
