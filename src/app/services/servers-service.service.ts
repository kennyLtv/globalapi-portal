import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ServersService {

  constructor(private http: HttpClient) { }

  private baseUrl:string = "https://auth.global-api.com/api/v1/server";
  private _limit:number = 100;

  set limit (limit:number) {
    this._limit = limit;
  }

  get limit() {
    return this._limit;
  }

  getServers() {

    let requestUrl:string = this.baseUrl + "?";

    if (this._limit > 0) {
      requestUrl += "limit=" + this._limit;
    }

    return this.http.get(requestUrl, { withCredentials: true, responseType: 'text' });
  }

  createServer(ip:string, port:number, name:string, owner_steamid64:string) {

    let requestUrl:string = this.baseUrl + "/create";
    let data:object = {"port": port, "ip": ip, "name": name, "owner_steamid64": owner_steamid64};

    return this.http.post(requestUrl, data, { withCredentials: true });
  }

  modifyServer(id:number, api_key?:string, name?:string, ip?:string, port?:number, owner_steamid64?:string, status?:number) {

    let requestUrl:string = this.baseUrl;
    let data:Object = {"id": id, "api_key": api_key, "name": name, "ip": ip, "port": port, "owner_steamid64": owner_steamid64, "approval_status": status};

    if (id > 0) {
      requestUrl += "/" + id;
    }
    return this.http.put(requestUrl, data, { withCredentials: true });
  }

  applyServer(ip:string, port:number, name:string) {

    let requestUrl:string = this.baseUrl + "/create";
    let data:object = {"port": port, "ip": ip, "name": name};

    return this.http.post(requestUrl, data, { withCredentials: true });
  }

  approveServer(id:number) {

    let requestUrl:string = this.baseUrl + "/Approve?";

    if (id > 0) {
      requestUrl += "id=" + id;
    }

    return this.http.post(requestUrl, undefined, { withCredentials: true });
  }

  rejectServer(id:number) {

    let requestUrl:string = this.baseUrl + "/Reject?";

    if (id > 0) {
      requestUrl += "id=" + id;
    }

    return this.http.post(requestUrl, undefined, { withCredentials: true });
  }

  deleteServer(id:number) {

    let requestUrl:string = this.baseUrl;

    if (id > 0) {
      requestUrl += "/" + id;
    }

    return this.http.delete(requestUrl, { withCredentials: true });
  }
}
