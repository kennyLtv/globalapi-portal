import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpXsrfTokenExtractor } from '@angular/common/http';

import { NotificationService } from './notification.service';

import { User } from './../models/user';

@Injectable()
export class LoginService {

  constructor(private router: Router,
              private http: HttpClient,
              private tokenExtractor: HttpXsrfTokenExtractor,
              private notificationService: NotificationService) { }

  private UserInfo = new User();

  public accessDenied:boolean = false;

  // ------------------ LOGIN LOGIC ------------------ //

  login() {
    return this.http.get("https://auth.global-api.com/Account/UserInfo", { withCredentials: true });
  }

  steamLogin() {
    this.buildandSubmitForms();
  }

  userActiveSession() {
    this.storeInfo();
  }

  userLoggedIn() {
    this.storeInfo();
    this.routeUser();
    this.welcomeUser();
  }

  userNotLoggedIn(returnUrl) {
    this.denyAccess();
    this.storeReturnUrl(returnUrl);
  }

  // ------------------ LOGOUT LOGIC ------------------ //

  logout() {
    this.http.post("https://auth.global-api.com/Account/LogoutSPA", undefined, { withCredentials : true }).subscribe(
    (data) => this.logoutFallback(),
    (err) => this.errorHandle(err));
  }

  private logoutFallback() {
    this.routeToHome();
    this.logoutSuccess();
    this.resetLocalStorage();
  }

  // ------------------ MISC ------------------ //

  setUser(user) {
    this.UserInfo = user;
  }

  isLoggedIn() {
    return this.UserInfo.isLoggedIn;
  }

  isGlobalAdmin() {
    return this.UserInfo.isGlobalAdmin;
  }

  isAccessDenied() {
    return this.accessDenied;
  }

  private denyAccess() {
    this.accessDenied = true;
    this.router.navigate(['/login']);
  }

  private welcomeUser() {
    this.notificationService.showSuccess(this.UserInfo.steamUserInfo['personaname'], "Welcome to GlobalAPI Portal");
  }

  private logoutSuccess() {
    this.notificationService.showSuccess("Logged out!", "")
  }

  private routeToHome() {
    this.router.navigate(['/login']);
  }

  private storeReturnUrl(returnUrl) {
    if (returnUrl != "/") {
      localStorage.setItem("returnUrl", returnUrl);
    }
  }

  private submitForm() {
    document.forms['loginForm'].submit();
  }

  private storeInfo() {
    localStorage.setItem("SteamID64", this.UserInfo.steamID64.toString());
    localStorage.setItem("ProfileIcon", this.UserInfo.steamUserInfo['avatar']);
    localStorage.setItem("ProfileName", this.UserInfo.steamUserInfo['personaname']);
  }

  private routeUser() {
    let returnUrl = localStorage.getItem("returnUrl");

    if (returnUrl != null) {
      this.router.navigate([returnUrl]);
      localStorage.removeItem("returnUrl");
    }

    else if (this.router.url !== "/") {
      this.router.navigate(['/']);
    }
  }

  private buildandSubmitForms() {
    let token = this.tokenExtractor.getToken();
    document.write('<form method="post" id="loginForm" action="https://auth.global-api.com/Account/ExternalLogin">');
    document.write('<input type="hidden" name="provider" value="Steam">');
    document.write('<input type="hidden" name="returnUrl" value="https://portal.global-api.com/login">');
    document.write('<input type="hidden" name="__RequestVerificationToken" value=' + token + ">");
    document.write('</form>');

    this.submitForm();
  }

  private resetLocalStorage() {
    localStorage.clear();
  }

  private errorHandle(err) {
    this.notificationService.showError("Something bad happened!", "");
    console.log(err);
  }
}
