import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RecordsService {

  constructor(private http: HttpClient) { }

  private baseUrl:string = "https://kztimerglobal.com/api/v1/records";
  private limit:number = 100;
  private mapName:string = undefined;
  private modeName:string = undefined;
  private tickRate:number = undefined;
  private steamId:string = undefined;
  private top100:number = undefined;
  private top100_overall:number = undefined;

  set recordLimit (limit:number) {
    this.limit = limit;
  }

  get recordLimit() {
    return this.limit;
  }

  set recordMap(map:string) {
    this.mapName = map;
  }

  get recordMap() {
      return this.mapName;
  }

  set recordMode(mode:string) {
      this.modeName = mode;
  }

  get recordMode() {
      return this.modeName;
  }

  set recordTickrate(tickrate:number) {
      this.tickRate = tickrate;
  }

  get recordTickrate() {
      return this.tickRate;
  }

  set recordSteamid(steamid:string) {
    this.steamId = steamid;
  }

  get recordSteamid() {
      return this.steamId;
  }

  set recordTop100(top:number) {
      this.top100 = top;
  }

  get recordTop100() {
      return this.top100;
  }

  set recordTop100Overall(top:number) {
      this.top100_overall = top;
  }

  get recordTop100Overall() {
      return this.top100_overall;
  }

  getRecordsSelf(steamid:string) {

    var requestUrl:string = this.baseUrl + "/top/?";

    if (this.modeName != undefined) {
        requestUrl += "modes=" + this.modeName + "&";
    }

    if (this.tickRate != undefined) {
        requestUrl += "tick_rate=" + this.tickRate + "&";
    }

    if (steamid != undefined) {
        requestUrl += "steamid64=" + steamid + "&";
    }

    if (this.limit > 0) {
        requestUrl += "limit=" + this.limit;
    }

    return this.http.get(requestUrl, { responseType: 'text' });
  }

  getRecentRecords() {
    
    var requestUrl:string = this.baseUrl + "/top/recent/?";

    if (this.modeName != undefined) {
        requestUrl += "modes=" + this.modeName + "&";
    }

    if (this.tickRate != undefined) {
        requestUrl += "tick_rate=" + this.tickRate + "&";
    }

    if (this.recordTop100 != undefined) {
        requestUrl += "top_100=" + this.recordTop100 + "&";
    }

    if (this.recordTop100Overall != undefined ) {
        requestUrl += "top_100_overall=" + this.recordTop100Overall + "&";
    }

    if (this.limit > 0) {
      requestUrl += "limit=" + this.limit;
    }

    return this.http.get(requestUrl, { responseType: 'text' });
  }
}