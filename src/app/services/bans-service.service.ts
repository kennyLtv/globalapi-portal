import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';

@Injectable()
export class BansService {

  constructor(private http: HttpClient) { }

  private publicAPI:string = "https://kztimerglobal.com/api/v1/bans";
  private baseUrl:string = "https://auth.global-api.com/api/v1/bans";
  private limit:number = 100;
  private isExpired:boolean = undefined;
  private notesContains:string = undefined;
  private statsContains:string = undefined;
  private serverId:number = undefined;

  set banLimit (limit:number) {
    this.limit = limit;
  }

  get banLimit() {
    return this.limit;
  }

  set banIsExpired(expired:boolean) {
    this.isExpired = expired;
  }

  get banIsExpired() {
    return this.isExpired;
  }

  set banNotes(notes:string) {
    this.notesContains = notes;
  }

  get banNotes() {
    return this.notesContains;
  }

  set banStats(stats:string) {
    this.statsContains = stats;
  }

  get banStats() {
    return this.statsContains;
  }

  set banServerId(id:number) {
    this.serverId = id;
  }

  get banServerId() {
    return this.serverId;
  }

  getBans(steamid?:string) {

    let requestUrl:string = this.publicAPI+ "?";

    if (steamid != undefined) {
      requestUrl += "steamid64=" + steamid + "&";
    }

    if (this.isExpired != undefined) {
      requestUrl += "is_expired=" + this.isExpired + "&";
    }

    if (this.notesContains != undefined) {
      requestUrl += "notes_contains=" + this.notesContains + "&";
    }

    if (this.statsContains != undefined) {
      requestUrl += "stats_contains=" + this.statsContains + "&";
    }

    if (this.serverId != undefined) {
      requestUrl += "server_id=" + this.serverId + "&";
    }

    if (this.limit > 0) {
      requestUrl += "limit=" + this.limit;
    }
    return this.http.get(requestUrl, { responseType: 'text' });
  }

  modifyBan(id:number, ban_type:string, expires_on:string, ip:string, steamid64:string, player_name:string, steam_id:string, notes:string, stats:string, server_id:number) {

    let requestUrl:string = this.baseUrl;

    if (id > 0) {
      requestUrl += "?" + id;
    }

    let data:Object = {"id": id, "ban_type": ban_type, "expires_on": expires_on, "ip": ip, "steamid64": steamid64, "player_name": player_name, "steam_id": steam_id, "notes": notes, "stats": stats, "server_id": server_id};

    return this.http.put(requestUrl, data, { withCredentials: true});
  }
}
