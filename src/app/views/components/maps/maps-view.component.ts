import { Component, OnInit } from '@angular/core';
import { MapsService } from '../../../services/maps-service.service';
import { NotificationService } from '../../../services/notification.service';

@Component({
  templateUrl: 'maps-view.component.html'
})

export class MapsViewComponent implements OnInit {

  constructor(private mapsService: MapsService,
              private notificationService: NotificationService) { }

  public currentPage:number;
  public mapsData: ArrayBuffer[] = [];

  ngOnInit() {
    this.mapsService.mapLimit = 500;
    this.mapsService.mapValidated = true;
    this.subscribeToData();
  }

  subscribeToData() {
    this.mapsService.getMaps().subscribe(
    (data) => this.pushData(data),
    (err) => this.errorFunction(err));
  }

  pushData(data) {
    this.mapsData = data;
  }

  errorFunction(err) {
    this.notificationService.showError("Maps", err.status + " " + err.statusText);
    console.log(err);
  }
}
