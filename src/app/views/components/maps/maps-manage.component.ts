import { Component, OnInit } from '@angular/core';
import { MapsService } from '../../../services/maps-service.service';
import { NotificationService } from '../../../services/notification.service';

@Component({
  templateUrl: 'maps-manage.component.html'
})

export class MapsManageComponent implements OnInit {

  constructor(private mapsService: MapsService,
              private notificationService: NotificationService) { }

  public currentPage:number;
  public oldData: Array<any> = [];
  public mapsData: Array<any> = [];

  public title = "Are you sure";
  public modifymsg = "You want to modify this map?";
  public deletemsg = "You want to DELETE this map?";
  public revertmsg = "You want to revert changes made to this map?";

  ngOnInit() {
    this.mapsService.mapLimit = 1000;
    this.mapsService.mapValidated = true;
    this.subscribeToMapsData();
  }
  
  toggleValidated() {
    this.mapsService.mapValidated = !this.mapsService.mapValidated;
    this.subscribeToMapsData();
  }

  addMap() {
    let oldLength = this.oldData.length;
    let newLength = this.mapsData.length;
    let difference = newLength - oldLength;

    if (oldLength !== newLength) {
      this.mapsData.splice(0, difference);
    }

    let nullMap:Object = {"id": -1, "name": "", "validated": false, "filesize": -1, "difficulty": -1, "approved_by_steamid64": -1};
    this.mapsData.unshift(nullMap);
  }

  dataModified(index):boolean {
    return JSON.stringify(this.mapsData[index]) !== JSON.stringify(this.oldData[index]);
  }

  modify(id:number) {    
    if (id == -1) {

      let data = this.mapsData[0]; // Always 0
      let name = data['name'].toString();
      let filesize = parseInt(data['filesize']);
      let approver = data['approved_by_steamid64'].toString();
      let difficulty = parseInt(data['difficulty']);
      let validated = data['validated'];

      this.subscribeToAddData(name, filesize, approver, difficulty, validated);
    }

    else {
      let index = this.mapsData.findIndex(x => x['id'] == id);

      if (this.dataModified(index)) {

        let data = this.mapsData[index];
        let name = data['name'].toString();
        let filesize = parseInt(data['filesize']);
        let approver = data['approved_by_steamid64'].toString();
        let difficulty = parseInt(data['difficulty']);
        let validated = data['validated'];

        this.subscribeToModifyData(id, name, filesize, approver, difficulty, validated);
      }
    }
  }

  revert(id:number) {

    let difference = this.mapsData.length - this.oldData.length;

    if (difference > 0) {
      
    }

    if (id == -1) {
      this.mapsData[0] = "";
      this.notificationService.showSuccess("Maps", "Reverted the changes to the map");
    }
    else {
      let index = this.mapsData.findIndex(x => x['id'] == id);
      this.mapsData[index] = this.oldData[index];
      this.notificationService.showSuccess("Maps", "Reverted the changes to the map");
    }
  }

  delete(id:number) {
    if (id == undefined) {
      this.mapsData.splice(0, 1);
    }
    else {
      this.subscribeToDeleteData(id);
    }
  }

  subscribeToMapsData() {
    this.mapsService.getMaps().subscribe(
    (data) => this.pushMapsData(data),
    (err) => this.errorFunction(err));
  }

  subscribeToModifyData(id:number, name:string, filesize:number, approver:string, difficulty:number, validated:boolean) {
    this.mapsService.modifyMap(id, name, filesize, approver, difficulty, validated).subscribe(
    (data) => this.pushModifyData(data),
    (err) => this.errorFunction(err));
  }

  subscribeToDeleteData(id:number) {
    this.mapsService.deleteMap(id).subscribe(
    (data) => this.pushDeleteData(data),
    (err) => this.errorFunction(err));
  }

  subscribeToAddData(name:string, filesize:number, approver:string, difficulty:number, validated:boolean) {
    this.mapsService.addMap(name, filesize, approver, difficulty, validated).subscribe(
    (data) => this.pushAddData(data),
    (err) => this.errorFunction(err));
  }

  pushMapsData(data) {
    this.mapsData = data;
    this.oldData = this.mapsData.map(x => Object.assign({}, x));
  }

  pushModifyData(data) {
    this.notificationService.showSuccess("Maps", "Successfully modified the map!");
    this.subscribeToMapsData();
  }

  pushDeleteData(data) {
    this.notificationService.showSuccess("Maps", "Successfully deleted the map!");
    this.subscribeToMapsData();
  }

  pushAddData(data) {
    this.notificationService.showSuccess("Maps", "Successfully deleted the map!");
    this.subscribeToMapsData();
  }

  errorFunction(err) {
    this.notificationService.showError("Maps", err.status + " " + err.statusText);
    console.log(err);
  }
}
