// Common
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '../../directives/directives.module';

// Pagination, Tooltip & Clipboard
import { NgxPaginationModule } from 'ngx-pagination';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ClipboardModule } from 'ngx-clipboard';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';

// Routing & Service
import { BansService } from '../../services/bans-service.service';
import { BansRoutingModule } from './bans-routing.module';

import { BansViewComponent } from './bans/bans-view.component';
import { BansManageComponent } from './bans/bans-manage.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    ClipboardModule,
    DirectivesModule,
    BansRoutingModule,
    NgxPaginationModule,
    TooltipModule.forRoot(),
    ConfirmationPopoverModule.forRoot()
  ],
  declarations: [
    BansViewComponent,
    BansManageComponent
   ],
   providers: [
    BansService
   ]
})

export class BansModule { }
