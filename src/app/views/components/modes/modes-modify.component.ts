import { Component, OnInit } from '@angular/core';
import { ModesService } from '../../../services/modes-service.service';
import { NotificationService } from '../../../services/notification.service';

@Component({
  templateUrl: 'modes-modify.component.html'
})

export class ModesModifyComponent implements OnInit {

  constructor(private modesService: ModesService,
              private notificationService: NotificationService) { }

  public currentPage:number;
  public oldData:ArrayBuffer[] = [];
  public modesData:ArrayBuffer[] = [];

  public title = "Are you sure";
  public modifymsg = "You want to modify this mode?";
  public revertmsg = "You want to revert changes made to this mode?";

  ngOnInit() {
    this.subscribeToModesData();
  }

  dataModified(index):boolean {
    return JSON.stringify(this.modesData[index]) !== JSON.stringify(this.oldData[index]);
  }

  modify(id:number) {
    let index = this.modesData.findIndex(x => x['id'] == id);

    if (this.dataModified(index)) {

      let data = this.modesData[index];
      let name = data['name'];
      let desc = data['description'];
      let latest_ver = data['latest_version'];
      let latest_ver_desc = data['latest_version_description'];
      let website = data['website'];
      let repo = data['repo'];
      let contactsid64 = data['contact_steamid64'];

      this.subscribeToModifyData(id, name, desc, latest_ver, latest_ver_desc, website, repo, contactsid64);
    }
  }

  revert(id:number) {
    console.log("Revert");
  }

  subscribeToModesData() {
    this.modesService.getModes().subscribe(
    (data) => this.pushModesData(data),
    (err) => this.errorHandle(err));
  }

  subscribeToModifyData(id, name, desc, latest_ver, latest_ver_desc, website, repo, contactsid64)  {
    this.modesService.modifyMode(id, name, desc, latest_ver, latest_ver_desc, website, repo, contactsid64).subscribe(
    (data) => this.pushModifyData(data),
    (err) => this.errorHandle(err));
  }

  pushModesData(data) {
    let pattern = /\"contact_steamid64\"\:([0-9]{17})\,/g;
    data = data.replace(pattern, '"contact_steamid64":"$1",');
  
    this.modesData = JSON.parse(data);                            // This is the two-way data binded data (changes)
    console.log(this.modesData);
    this.oldData = this.modesData.map(x => Object.assign({}, x)); // copy of unmodified modes data
  }

  pushModifyData(data) { }

  errorHandle(err) {
    console.log(err);
    this.notificationService.showError("Modes", err.status + " " + err.statusText);
  }
}
