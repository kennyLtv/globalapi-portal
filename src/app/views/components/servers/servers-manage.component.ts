import { Component, OnInit } from '@angular/core';
import { ServersService } from '../../../services/servers-service.service';
import { NotificationService } from '../../../services/notification.service';

import { Server } from '../../../models/servers-manage';

@Component({
  templateUrl: 'servers-manage.component.html'
})

export class ServersManageComponent implements OnInit {

  constructor(private serversService: ServersService,
              private notificationService: NotificationService) { }

  public ready:boolean;
  public currentPage:number;

  public title = "Are you sure";
  public rejectmsg = "You want to reject this server?";
  public deletemsg = "You want to DELETE this server?";
  public approvemsg = "You want to approve this server?";

  public server = new Server();
  public serversData: ArrayBuffer[] = [];

  ngOnInit() {
    this.subscribeToServerData();
  }

  steamIdToName(steamid:string) {
    switch(steamid) {
      case "76561198003275951": return "Sikari";
      case "76561197977956420": return "Zpamm";
      case "76561197993465458": return "Chuckles";
      case "76561197983014207": return "Zach47";
      case "76561197989817982": return "DanZay";
      default: return "N/A";
    }
  }

  approve(id:number) {
    this.subscribeToApproveData(id);
  }

  reject(id:number) {
    this.subscribeToRejectData(id);
  }

  delete(id:number) {
    this.subscribeToDeleteData(id);
  }

  subscribeToApproveData(id:number) {
    this.serversService.approveServer(id).subscribe(
    (data) => this.pushApproveData(data),
    (err) => this.errorFunction(err));
  }

  subscribeToRejectData(id:number) {
    this.serversService.rejectServer(id).subscribe(
    (data) => this.pushRejectData(data),
    (err) => this.errorFunction(err));
  }

  subscribeToDeleteData(id:number) {
    this.serversService.deleteServer(id).subscribe(
    (data) => this.pushDeleteData(data),
    (err) => this.errorFunction(err));
  }

  subscribeToServerData() {
    this.serversService.getServers().subscribe(
    (data) => this.pushServerData(data),
    (err) => this.errorFunction(err));
  }

  pushApproveData(data) {
    this.notificationService.showSuccess("Servers", "Successfully approved the server!");
    this.subscribeToServerData();
  }

  pushRejectData(data) {
    this.notificationService.showSuccess("Servers", "Successfully rejected the server!");
    this.subscribeToServerData();
  }

  pushDeleteData(data) {
    this.notificationService.showSuccess("Servers", "Successfully deleted the server!");
    this.subscribeToServerData();
  }

  pushServerData(data) {
    let pattern = /\"owner_steamid64\"\:([0-9]{17})\,/g;
    let pattern2 = /\"approved_by_id\"\:([0-9]{17})/g;

    data = data.replace(pattern, '"owner_steamid64":"$1",');
    data = data.replace(pattern2, '"approved_by_id":"$1"');
    
    this.serversData = JSON.parse(data);
    this.ready = true;
  }

  errorFunction(err) {
    console.log(err);
    this.notificationService.showError("Servers", err.status + " " + err.statusText);
  }
}
