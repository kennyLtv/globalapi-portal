import { Component } from '@angular/core';
import { ServersService } from '../../../services/servers-service.service';
import { NotificationService } from '../../../services/notification.service';

@Component({
  templateUrl: 'servers-view.component.html',
})

export class ServersViewComponent {

  constructor(private serversService: ServersService,
              private notificationService: NotificationService) { }

  public ready:boolean;
  public currentPage:number;
  public serversData: ArrayBuffer[] = [];

  ngOnInit() {
    this.subscribeToData();
  }

  steamIdToName(steamid:string) {
    switch(steamid) {
      case "76561198003275951": return "Sikari";
      case "76561197977956420": return "Zpamm";
      case "76561197993465458": return "Chuckles";
      case "76561197983014207": return "Zach47";
      case "76561197989817982": return "DanZay";
      default: return "N/A";
    }
  }

  copySuccess() {
    this.notificationService.showSuccess("API Key", "Succesfully copied to clipboard");
  }

  subscribeToData() {
    this.serversService.getServers().subscribe(
    (data) => this.pushData(data),
    (err) => this.errorFunction(err));
  }

  pushData(data) {
    let pattern = /\"owner_steamid64\"\:([0-9]{17})\,/g;
    let pattern2 = /\"approved_by_steamid64\"\:([0-9]{17})/g;

    data = data.replace(pattern, '"owner_steamid64":"$1",');
    data = data.replace(pattern2, '"approved_by_steamid64":"$1"');

    this.serversData = JSON.parse(data);
    this.ready = true;
  }

  errorFunction(err) {
    console.log(err);
    this.notificationService.showError("Servers", err.status + " " + err.statusText);
  }
}
