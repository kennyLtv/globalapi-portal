import { Component } from '@angular/core';
import { ServersService } from '../../../services/servers-service.service';
import { NotificationService } from '../../../services/notification.service';

import { Server } from '../../../models/servers-apply';

@Component({
  templateUrl: 'servers-apply.component.html'
})

export class ServersApplyComponent {

  constructor(private serversService: ServersService,
              private notificationService: NotificationService) { }

  public server = new Server();
  public formSubmitted = false;

  public title = "Are you sure";
  public message = "You want to apply for this server?";

  apply() {

    var ip = this.server.ip;
    var port = this.server.port;
    var name = this.server.name;

    this.subscribeToData(ip, port, name);
  }

  subscribeToData(ip:string, port:number, name:string) {
    this.serversService.applyServer(ip, port, name).subscribe(
    (data) => this.pushData(data),
    (err) => this.errorFunction(err));
  }

  pushData(data) {
    this.notificationService.showSuccess("Servers", "Successfully applied for the server!");
    this.formSubmitted = true;
  }

  errorFunction(err) {
    console.log(err);
    this.notificationService.showError("Servers", err.status + " " + err.statusText);
  }
}
