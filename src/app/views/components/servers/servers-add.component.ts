import { Component } from '@angular/core';
import { ServersService } from '../../../services/servers-service.service';
import { NotificationService } from '../../../services/notification.service';

import { Server } from '../../../models/servers-add';

@Component({
  templateUrl: 'servers-add.component.html'
})

export class ServersAddComponent {

  constructor(private serversService: ServersService,
              private notificationService: NotificationService) { }

  public server = new Server();
  public formSubmitted = false;

  public title = "Are you sure";
  public message = "You want to add this server?";

  addServer() {
    var ip = this.server.ip;
    var port = this.server.port;
    var name = this.server.name;
    var owner_steamid64 = this.server.ownersid64;

    this.subscribeToData(ip, port, name, owner_steamid64);
  }

  subscribeToData(ip:string, port:number, name:string, owner_steamid64:string) {
    this.serversService.createServer(ip, port, name, owner_steamid64).subscribe(
    (data) => this.pushData(data),
    (err) => this.errorFunction(err));
  }

  pushData(data) {
    this.notificationService.showSuccess("Servers", "Successfully added the server!");
    this.formSubmitted = true;
  }

  errorFunction(err) {
    console.log(err);
    this.notificationService.showError("Servers", err.status + " " + err.statusText);
  }
}
