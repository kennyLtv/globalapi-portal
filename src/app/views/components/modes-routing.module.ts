import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModesModifyComponent } from './modes/modes-modify.component';

import { AdminAuthGuard } from '../../guards/admin-auth.guard';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Modes'
    },
    children: [
      {
        path: 'modify',
        component: ModesModifyComponent,
        canActivate: [AdminAuthGuard],
        data: {
          title: 'Modify'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModesRoutingModule {}
