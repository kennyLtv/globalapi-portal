import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RecordsService } from '../../../services/records-service.service';
import { NotificationService } from '../../../services/notification.service';

@Component({
  templateUrl: 'records-view.component.html'
})

export class RecordsViewComponent implements OnInit {

  constructor(private router: Router,
              private recordService: RecordsService,
              private notificationService: NotificationService) { }

  public ready:boolean;
  public selfRoute:boolean;
  public currentPage:number;
  public recordsData: ArrayBuffer[] = [];
  public steamid = localStorage.getItem("SteamID64");
  public recordsTitle:string;

  ngOnInit() {

    this.recordService.recordTickrate = 128;
    this.recordService.recordTop100 = 100;

    if (this.router.url.endsWith("/me")) {
      this.SelfRoute();
    }

    else {
      this.NormalRoute();
    }
  }

  SelfRoute() {
    this.selfRoute = true;
    this.recordsTitle = "Your Records";
    this.subscribeToSelfData();
  }

  NormalRoute() {
    this.selfRoute = false;
    this.recordsTitle = "Recent Records";
    this.subscribeToData();
  }

  formatRecordTime(time:number) {
    var date = new Date(null);
    date.setSeconds(time);
    var dateString = date.toISOString().substr(11, 8);
    var milliseconds = time.toString().split(".")[1];

    return dateString + "." + milliseconds;
  }

  formatRecordDate(datetime:string) {
    let time = datetime.substr(11,10);
    let date = datetime.substr(0,10);
    return date + " (" + time + ")";
  }

  subscribeToData() {
    let data = this.recordService.getRecentRecords().subscribe(
    (data) => this.pushData(data),
    (err) => this.errorFunction(err));
  }

  subscribeToSelfData() {
    let data = this.recordService.getRecordsSelf(this.steamid).subscribe(
    (data) => this.pushData(data),
    (err) => this.errorFunction(err));
  }

  pushData(data) {
    let pattern = /\"steamid64\"\:([0-9]{17})\,/g;
    data = data.replace(pattern, '"steamid64":"$1",');

    this.recordsData = JSON.parse(data);
    this.ready = true;
  }

  errorFunction(err) {
    this.notificationService.showError("Records", err.status + " " + err.statusText);
  }
}
