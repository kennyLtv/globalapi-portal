import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RecordsAddComponent } from './records/records-add.component';
import { RecordsDeleteComponent } from './records/records-delete.component';
import { RecordsModifyComponent } from './records/records-modify.component'; 
import { RecordsViewComponent } from './records/records-view.component';

import { AdminAuthGuard } from '../../guards/admin-auth.guard';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Records'
    },
    children: [
      {
        path: 'add',
        component: RecordsAddComponent,
        canActivate: [AdminAuthGuard],
        data: {
          title: 'Add'
        }
      },
      {
          path: 'delete',
          component: RecordsDeleteComponent,
          canActivate: [AdminAuthGuard],
          data: {
            title: 'Delete'
          }
      },
      {
          path: 'modify',
          component: RecordsModifyComponent,
          canActivate: [AdminAuthGuard],
          data: {
            title: 'Modify'
          }
      },
      {
        path: 'view',
        component: RecordsViewComponent,
        data: {
          title: 'View'
        }
      },
      {
        path: 'view/me',
        component: RecordsViewComponent,
        data: {
          title: 'View'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecordsRoutingModule {}
