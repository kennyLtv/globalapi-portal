import { NgModule } from '@angular/core';
import { EditableModelDirective } from './custom/editable/editable.directive';

@NgModule({
  imports: [],
  declarations: [ EditableModelDirective ],
  exports: [ EditableModelDirective ]
})

export class DirectivesModule { }