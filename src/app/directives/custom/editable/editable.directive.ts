import {
    Directive,
    ElementRef,
    Input,
    Output,
    OnChanges,
    EventEmitter,
    HostListener,
    SimpleChanges
} from '@angular/core';

@Directive({
    selector: '[editableModel]'
})

export class EditableModelDirective implements OnChanges {

    @Input('editableModel')
    public model: any;

    @Output('editableModelChange')
    public update = new EventEmitter();

    private _lastViewModel: any;

    constructor(private elRef: ElementRef) {}

    public ngOnChanges(changes: SimpleChanges): void {
        if(this._lastViewModel !== changes['model'].currentValue){
            this._lastViewModel = this.model;
            this._refreshView();
        }
    }

    @HostListener('blur')
    public onBlur() {
        var value = this.elRef.nativeElement.innerText;
        this._lastViewModel = value;
        this.update.emit(value);
    }

    private _refreshView() {
        this.elRef.nativeElement.innerText = this.model;
    }
}