export class User {

    constructor(public steamID64?: number,
                public isLoggedIn?: boolean,
                public isGlobalAdmin?: boolean,
                public steamUserInfo?: object[]) { }
}